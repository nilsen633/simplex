#! usr/bin/env python3
import os
import sys
import numpy as np
import copy
import pdb

class Arc(object):
    def __init__(self, index, capacity, cost, outgoing, incoming, flow = 0, potential = 0):
        self.index = index
        self.capacity = capacity
        self.cost = cost
        self.outgoing = outgoing
        self.incoming = incoming
        self.flow = flow
        self.potential = potential
        self.status = 'L'
        
        
    def __str__(self):
        return('(' + str(self.outgoing) + ', ' + str(self.incoming) + ')')

class Node(object):
    def __init__(self, index, balance, potential = 0, depth = 0, pred = 0, thread = 0):
        self.index = index
        self.balance = balance
        self.potential = potential
        self.depth = depth
        self.pred = pred
        self.thread = thread
        self.delta_plus = []
        self.delta_minus = []
        self.color = "white"
    
    def __str__(self):
        return str(self.index)

        
class ReadGraph(object):
    
    def __init__(self, infile):
        self.path = os.getcwd() + "/sets/" + infile
        self.instance = open(self.path).readlines()
        self.nodes = []
        self.arcs = []
        self.information = []
        self.A = self.read_Graph()
        
    def __str__(self):
        for node in self.nodes:
           print ('Node: ' + str(node.index) + ', b: ' + str(node.balance) + ' pot: ' + str(node.potential) + ' depth: ' + str(node.depth) + ' pred: ' + str(node.pred) + ' thread: ' + str(node.thread))
           print('')
        for arc in self.arcs:
        	print ('Arc: ' + str(arc.status) + ', (' + str(arc.outgoing) +', '+ str(arc.incoming) +')'+ ', u: ' + str(arc.capacity) + ', c: ' + str(arc.cost) +' cPi: ' + str(arc.potential) + ', f: ' + str(arc.flow))
        return ''
        
    def read_Graph(self):
        i = 1
        for line in self.instance:
            linesplit = line.split()
            if line[0] == "p":
                self.information =([linesplit[1], int(linesplit[2]), int(linesplit[3])])
            if line[0] == "n":
                node = Node(int(linesplit[1]), int(linesplit[2]))
                self.nodes.append(node)
                print(node)
            if line[0] == "a":
                arc = Arc(i, int(linesplit[4]), int(linesplit[5]), int(linesplit[1]), int(linesplit[2]))
                print(arc)
                i += 1
                self.nodes[arc.outgoing-1].delta_plus.append(self.nodes[arc.incoming-1])
                self.nodes[arc.incoming-1].delta_minus.append(self.nodes[arc.outgoing-1])
                self.arcs.append(arc)
                arc.status = 'L'
        n = self.information[1]
        A = [[None for k in range(n + 1)] for l in range(n + 1)]
        return A
                    
def return_feasible_sol(graph):  #(self, index, capacity, cost, outgoing, incoming, flow = 0, depth = 0, pred = 0, thread = 0):
    M = 1
    for arc in graph.arcs:
        M += abs(arc.cost)
    root = Node(0, 0, 0, 0, 0, 1)                     # new root node with b(0) = 0 irrelevant
    i = 0
    for node in graph.nodes:
        i -= 1
        if node.balance < 0:
            arc = Arc(i, float('inf'), M, 0, node.index, -node.balance)
            graph.arcs.append(arc)
            graph.A[0][node.index] = arc
            arc.status = 'T'
            node.potential = 0 - M
            node.depth = 1
            node.pred = root
            if node.index != len(graph.nodes):
                node.thread = graph.nodes[node.index]
            root.delta_plus.append(node)
            node.delta_minus.append(root)
        if node.balance >= 0:
            arc = Arc(i, float('inf'), M, node.index, 0, node.balance)
            graph.arcs.append(arc)
            graph.A[node.index][0] = arc
            arc.status = 'T'
            node.potential = 0 + M
            node.depth = 1
            node.pred = root
            if node.index != len(graph.nodes):
                node.thread = graph.nodes[node.index]  
            node.delta_plus.append(root)
            root.delta_minus.append(node) 
    graph.nodes[-1].thread = root
    root.pred = None
    root.thread = graph.nodes[0] 
    graph.nodes.insert(0, root)
    return

def compute_node_potentials(graph):
  #  pdb.set_trace()
    root = graph.nodes[0]
    root.potential = 0
    node = root.thread
    while node != root:
        prenode = node.pred
        (arc, rev) = find_treearc(graph, prenode.index, node.index)
        if rev == 1:    
                node.potential = prenode.potential - arc.cost
        else:
                node.potential = prenode.potential + arc.cost
        node = node.thread
    return

def compute_arc_potentials(graph):
    pivot = Arc(0, 0, 0, 0, 0, -1)
    for arc in graph.arcs:
        arc.potential = arc.cost - graph.nodes[arc.outgoing].potential + graph.nodes[arc.incoming].potential
        if (arc.status == 'L' and arc.potential < 0) or (arc.status == 'U' and arc.potential > 0):
            if abs(arc.potential) > abs(pivot.potential):
                pivot = arc
    return pivot
        
                
def compute_flows(graph):
  #  pdb.set_trace()
    b = []
    T = []
    cgraph = copy.deepcopy(graph)
    for node in graph.nodes:
        b.append(node.balance)
        node.color = "white"
    for arc in cgraph.arcs:
        if arc.status == "U":
            arc.flow = arc.capacity
            b[arc.outgoing] -= arc.capacity 
            b[arc.incoming] += arc.capacity
        if arc.status == "L":
            arc.flow = 0
    root = cgraph.nodes[0]
    l = root.thread
    while l != root:
        (arc, rev) = find_treearc(cgraph, l.index, l.pred.index)
        T.append(arc)
        l = l.thread
    i = cgraph.nodes[0]


    while T != []:

        i = find_leave(root)
        #i is a node
        if i.pred == None:
            break
        else:
            x = i.pred

        arc = cgraph.A[i.index][x.index]
        if type(arc) == Arc:
            if arc.status == 'T':
                arc.flow = b[i.index]
            else:
                arc = cgraph.A[x.index][i.index]
                arc.flow = -b[i.index]
        else:
            arc = cgraph.A[x.index][i.index]
            arc.flow = -b[i.index]
        b[x.index] += b[i.index]
            #delete node with incident arc
        for y in range(len(T)):
            if T[y].incoming == i.index or T[y].outgoing == i.index:
                T.pop(y)
                break

    for k in range(len(cgraph.arcs)):
        graph.arcs[k].flow = cgraph.arcs[k].flow


    return


def find_leave(root):
    z = root
    i = z.thread
    j = i.thread
    while i.depth < j.depth:
        z = i
        i = j
        j  = j.thread
    z.thread = j
    return i

def find_treearc(graph, i, j):
    arc = graph.A[i][j]
    if type(arc) == Arc:
        return arc, 1
    else:
        arc = graph.A[j][i]
        return arc, -1

def find_leaving(cycle, orientation, delta = float('inf')):
   # pdb.set_trace()
    for i in range(len(cycle)):
        if orientation[i] == 1:
            a = cycle[i].capacity - cycle[i].flow
            if a < delta:
                delta = a
                leaving = cycle[i]
        else:
            a = cycle[i].flow
            if a < delta:
                delta = a
                leaving = cycle[i]
    j = len(cycle) - 1
    while j >= 0:
        if orientation[j] == 1:
            if cycle[j].capacity - cycle[j].flow == delta:
                return cycle[j], 'upper'
        else:
            if cycle[j].flow == delta:
                return cycle[j], 'lower'
        j -= 1     

def update_potentials(graph, entering, leaving):
   # pdb.set_trace()
    if graph.nodes[leaving.outgoing].depth < graph.nodes[leaving.incoming].depth:
        y = graph.nodes[leaving.incoming]
    else:
        y = graph.nodes[leaving.outgoing]                               # y becomes root of subtree T2
    if in_T1(graph, graph.nodes[entering.outgoing], y) == True:
        change = - entering.potential
    else:
        change = entering.potential
    y.potential += change
    z = y.thread
    while z.depth > y.depth:
        z.potential += change
        z = z.thread
    return

def in_T1(graph, start, y): # checks if a node start meets y on the way up
    x = start
    while x.index != 0:
        if x == y:
            return False
        x = x.pred
    return True
    


def identify_cycle(graph, entering_arc):
 #   pdb.set_trace()
    i = copy.deepcopy(graph.nodes[entering_arc.outgoing])
    j = copy.deepcopy(graph.nodes[entering_arc.incoming])
    cycle = []
    cycle1 = []
    cycle2 = []
    orientation = [] # 1 same orientation as cycle, 0 other
    orientation1 = []
    orientation2 = []
    if entering_arc.status == 'L':
        rev = False
    else:
        rev = True
    while i.index != j.index:
        if i.depth > j.depth:
            (arc, reverse) = find_treearc(graph, i.index, i.pred.index)
            i = i.pred
            cycle1.append(arc)
            if entering_arc.status == 'L':
                orientation1.append(reverse*(-1))
            else:
                orientation1.append(reverse)
        elif j.depth > i.depth:
            (arc, reverse) = find_treearc(graph, j.index, j.pred.index)
            j = j.pred
            cycle2.append(arc)
            if entering_arc.status == 'L':
                orientation2.append(reverse)
            else:
                orientation2.append(reverse*(-1))
        else:
    
            (arc, reverse) = find_treearc(graph, i.index, i.pred.index)

            cycle1.append(arc)
            if entering_arc.status == 'L':
                orientation1.append(reverse*(-1))
            else:
                orientation1.append(reverse)
            (arc, reverse) = find_treearc(graph, j.index, j.pred.index)
            cycle2.append(arc)
            if entering_arc.status == 'L':
                orientation2.append(reverse)
            else:
                orientation2.append(reverse*(-1))
            i = i.pred
            j = j.pred
    apex = j
    ent_arc = []
    ent_arc.append(entering_arc)
    if rev == False:
        cycle1 = cycle1[::-1]
        cycle = cycle1 + ent_arc + cycle2
        orientation1 = orientation1[::-1]
        orientation = orientation1 + [1] + orientation2
    else:
        cycle2 = cycle2[::-1]
        cycle = cycle2 + ent_arc + cycle1
        orientation2 = orientation2[::-1]
        orientation = orientation2 + [-1] + orientation1

    return cycle, orientation, apex

def last_under_subtree(k):
    y = k
    x = k.thread
    while x.depth > k.depth:
        y = x
        x = x.thread
    return y, x

def update_tree(graph, entering, leaving, info):
  #  pdb.set_trace()
    if entering == leaving:
        if info == 'lower':
            leaving.status = 'L'
        else:
            leaving.status = 'U'  
    else:
        update_potentials(graph, entering, leaving)
        if info == 'lower':
            leaving.status = 'L'
        else:
            leaving.status = 'U'
        entering.status = 'T'
        graph.A[leaving.outgoing][leaving.incoming] = None
        graph.A[entering.outgoing][entering.incoming] = entering

    brute_force_aktualisierung(graph, entering, leaving)
    return

'''
    if graph.nodes[leaving.outgoing].depth < graph.nodes[leaving.incoming].depth:
        q = graph.nodes[leaving.incoming]
        p = graph.nodes[leaving.outgoing]
    else:
        q = graph.nodes[leaving.outgoing]
        p = graph.nodes[leaving.incoming]
    
    if in_T1(graph, graph.nodes[entering.outgoing], q) == False:
        l = graph.nodes[entering.outgoing]
        k = graph.nodes[entering.incoming]
    else:
        l = graph.nodes[entering.incoming]
        k = graph.nodes[entering.outgoing]
    (k1, k2) = last_under_subtree(k)
    k1.thread = l
    x = l
    counter = 1

    while x != q:
        (u, z) = next_tree(x, k, counter) # u last node under l, z ist alter u.thread, ausserdem wird depth im aktuellen teilbaum aktualisiert
        x.depth = k.depth + counter
        u.thread = x.pred
        x.pred.thread = z
        x.pred.pred = x
        if x.pred == q:
            (q1, q2) = last_under_subtree(q)
            q1.thread = k2
            p.thread = q2
        x = u.thread
        counter += 1
    l.pred = k


'''

#brauchen wir für den speziellen fall von l nach q in T2
def next_tree(x, k, counter):
    u = x
    w = x.thread
    while w.depth > x.depth:
        w.depth = k.depth + counter + (w.depth - x.depth)
        u = w
        w = w.thread
    return u, w
    

def brute_force_aktualisierung(graph, entering_arc, leaving_arc): #stupide Tiefensuche


    root = graph.nodes[0]
    stack = []
    stack.append(root)
    while stack != []:

        node = stack[-1]
        if node.color == "white":
            node.color = "orange"
            for n in node.delta_minus:
                if graph.A[n.index][node.index] != None and n.color == "white":
                    stack.append(n)
                    n.pred = node
                    n.depth = node.depth + 1
            for n in node.delta_plus:
                if graph.A[node.index][n.index] != None and n.color == "white":
                    stack.append(n)
                    n.pred = node
                    n.depth = node.depth + 1
            if node != root:
                last_orange_node.thread = node
            last_orange_node = node

        elif node.color == "orange":
            node.color = "red"
        else:
            stack.pop()


    last_orange_node.thread = root

    return




def network_simplex(graph):
    #pdb.set_trace()
    return_feasible_sol(graph)
    #print(graph)
    compute_node_potentials(graph)
   # print(graph)
    entering_arc = compute_arc_potentials(graph)
   # print(graph)
    while entering_arc.flow != -1:
        (cycle, orientation, apex) = identify_cycle(graph, entering_arc)
      #  print(graph)
        (leaving_arc, info) = find_leaving(cycle, orientation)
      #  print(graph)
        update_tree(graph, entering_arc, leaving_arc, info)
      #  print(graph)
        compute_node_potentials(graph)
      #  print(graph)
        entering_arc = compute_arc_potentials(graph)
      #  print(graph)
        compute_flows(graph)
      #  print(graph)
    result = 0
    for arc in graph.arcs:
        if arc.index < 0:
            if arc.flow > 0:
                return "infisible"
        else:
            result += arc.flow * arc.cost
    return result

instance = ReadGraph(sys.argv[1])
print(network_simplex(instance))

'''
T=[]
U=[]
L=[]

for arc in instance.arcs:
    if arc.status == 'T':
        T.append(str(arc.outgoing) + " " + str(arc.incoming))
    if arc.status == 'U':
        U.append(str(arc.outgoing) + " " + str(arc.incoming))
    else:
        L.append(str(arc.outgoing) + " " + str(arc.incoming))

print("T: ", T)
print("U: ", U)
print("L: ", L)

for node in instance.nodes:
    print(node, "thread: ", node.thread, "depth: ", node.depth, "pred: ", node.pred)

'''
